var mesh, renderer, scene, camera, controls, clouds = [], planeMesh;

var cloudMat = new THREE.MeshBasicMaterial({color:0xffffff,shading:THREE.FlatShading,wireframe:false, depthWrite:false, transparent:true, opacity:0.1, fog:true});

var options = {
  speed: 0.5,
  cloudScale: 1.7,
  groundScale: 1
};


var gui = new dat.GUI();
gui.add(options, 'speed', 0, 3);
gui.add(options, 'cloudScale', 0.5, 5 ).onChange(function(){
  for ( var i = 0; i < clouds.length; i++ ){
    clouds[ i ].scale.set( options.cloudScale, 1, options.cloudScale );
  }
});


function generatePlane() {
  // var data = [
  //   0, 0, 1, 0, 0,
  //   1, 1, 2, 1, 1,
  //   0, 0, 1, 0, 0,
  //   0, 1, 1, 1, 0,
  //   0, 0, 1, 0, 0,
  // ];
  // var geometry = new VoxelMesh( data, 5, 5, false ).make();
  var data = [
    0, 0, 0, 1, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1,
    0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0,
    0, 0, 1, 1, 1, 0, 0,
    0, 0, 0, 1, 0, 0, 0,
  ];
  var geometry = new VoxelMesh( data, 7, 6, false ).make();

  var mat = new THREE.MeshBasicMaterial({color:0xFF0000, shading:THREE.FlatShading, wireframe:false, transparent:false, fog:true});



  var mesh = new THREE.Mesh( geometry, mat );

  return mesh;
}

function generateGround( width, height, depth ){
  var canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = depth;
  fastPerlin.perlinNoise(canvas);
  // should be calculated eventually.. triangles needed should not exceed max size of Uint16Array
  var chunkSize = 10;
  var groups = ( width * depth  ) / ( chunkSize * chunkSize );
  var container = new THREE.Group();
  var x = 0, z = 0;
  var xOffset = -width * 0.5;
  var zOffset = -depth * 0.5;
  var mat = new THREE.MeshBasicMaterial({color:0xffffff, vertexColors: THREE.VertexColors, shading:THREE.FlatShading, wireframe:false, transparent:false, fog:true});



  var pixelData = canvas.getContext('2d').getImageData( 0, 0, width, depth );
  // get min/max to normalize data
  var min = Number.MAX_VALUE;
  var max = 0;
  for( var i = 0; i < pixelData.data.length; i += 4 ){
    min = Math.min( pixelData.data[ i ], min );
    max = Math.max( pixelData.data[ i ], max );
  }

  if ( min === max ){
    max++;
  }

  for ( var k = 0; k < groups; k++ ){
    var perlinMap = canvas.getContext('2d').getImageData( x, z, chunkSize, chunkSize );

    var perlinArr = [];

    // normalize and hight decay (from center)
    var diff = max - min;

    for( i = 0; i < perlinMap.data.length; i += 4 ){
      var i_4 = i / 4;
      var y = Math.floor( ( ( perlinMap.data[ i ] - min ) / diff ) * height );
      perlinArr[ i_4 ] = Math.max(1,y);
    }

    var geometry = new VoxelMesh( perlinArr, chunkSize, chunkSize, false ).make();
    geometry.dynamic = true;
    var vertices = geometry.attributes.position.array;
    var colors = new Float32Array( vertices.length );
    for ( var c = 0; c < colors.length; c+= 3 ){
      var yc = ( vertices[ c + 1 ] + 0.5 ) / height;
      if ( yc > 0.2 ){
        colors[ c     ] = yc;
        colors[ c + 1 ] = yc;
      } else {
        //water
        colors[ c     ] = 0.05;
        colors[ c + 1 ] = 0.11;
        colors[ c + 2 ] = 0.20;
      }


      //if ( x === 0 ) console.log(yc);

    }
    geometry.addAttribute( 'color', new THREE.BufferAttribute(  colors, 3 ) );
    geometry.attributes.color.needsUpdate = true;

    var mesh = new THREE.Mesh( geometry, mat );
    mesh.position.set( xOffset + x, 0, zOffset + z );
    //mesh.rotation.y = Math.PI;
    container.add( mesh );
    x += chunkSize;
    if ( x === width ){
      z += chunkSize;
      x = 0;
    }
  }

  gui.add(options, 'groundScale',0.1, 10).onChange(function(val){
    container.scale.set(val,val,val);
  });

  return container;
}

function generateCloud(){
  var canvas = document.createElement('canvas');
  canvas.width = 20;
  canvas.height = 20;
  fastPerlin.perlinNoise(canvas);

  var perlinMap = canvas.getContext('2d').getImageData(0,0,20,20);
  var perlinArr = [];

  // get min/max to normalize data
  var min = Number.MAX_VALUE;
  var max = 0;
  for( var i = 0; i < perlinMap.data.length;i += 4 ){
    min = Math.min( perlinMap.data[ i ], min );
    max = Math.max( perlinMap.data[ i ], max );
  }

  // normalize and hight decay (from center)
  var center = { x: perlinMap.width * 0.5, y: perlinMap.height * 0.5 };
  var maxDist = Math.sqrt( center.x * center.x + center.y * center.y );

  for( i = 0; i < perlinMap.data.length; i += 4 ){
    var i_4 = i / 4;
    var x = i_4 % perlinMap.width;
    var y = Math.floor( i_4 / perlinMap.width );
    var dx = center.x - x;
    var dy = center.y - y;
    var dist = Math.sqrt( dx * dx + dy * dy );
    dist /= maxDist * 0.5;

    // only interested in one component/color since it's a b/w image
    perlinArr[ i_4 ] = ( ( 1 - dist ) * ( perlinMap.data[ i ] - min ) / ( max - min ) * 10 );
  }

  var geometry = new VoxelMesh( perlinArr, perlinMap.width, perlinMap.height, true ).make();
  var mesh = new THREE.Mesh( geometry, cloudMat )

  return mesh;

}

function generateClouds( num, y ){
  for ( var i = 0; i < num; i++ ){
    var cloud = generateCloud();
    cloud.velocityZ = 0.01 + Math.random() *0.03;
    scene.add( cloud );
    clouds.push( cloud );
    cloud.scale.set( options.cloudScale, 1, options.cloudScale );
    cloud.position.set( Math.random() * 200 - 100, y + Math.random() * ( y * 0.3 ), Math.random() * 200 - 100 );
  }
}


init();
animate();

function init() {

  // renderer
  renderer = new THREE.WebGLRenderer();
  renderer.setSize( window.innerWidth, window.innerHeight );

  document.body.appendChild( renderer.domElement );

  // scene
  scene = new THREE.Scene();
  scene.fog =  new THREE.FogExp2( 0x5c9dca, 0.0125 );

  // camera
  camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 10000 );
  camera.position.set( 0, 20, -80 );

  scene.add( camera );

  // controls
  controls = new THREE.OrbitControls( camera, renderer.domElement );
  controls.enableDamping = true;

  controls.minDistance = 10;
  controls.maxDistance = 100;


  controls.target = new THREE.Vector3( 0, 0, 0 );
  // ambient
  scene.add( new THREE.AmbientLight( 0xffffff, 0.8 ) );

  // ground
  scene.add( generateGround( 200, 12, 200 ) );

  // clouds!
  generateClouds( 200, 40 );

  // plane!
  planeMesh = generatePlane();
  scene.add( planeMesh  );
  planeMesh.position.set(0,16,0);

}

function animate() {
  var time = Date.now() * 0.001;
  requestAnimationFrame( animate );

  for ( var i = 0; i < clouds.length; i++ ){
    var c = clouds[ i ];
    c.position.z -= c.velocityZ * options.speed;
    if ( c.position.z < -100 ){
      c.position.z = 100;
    }
  }

  controls.update(); // required if damping = true
  renderer.setClearColor( 0x5c9dca, 1 );
  renderer.render( scene, camera );
  planeMesh.position.z = Math.cos( time * options.speed ) * 100;
  planeMesh.position.y = 20+Math.cos( time * options.speed * 0.1 ) * 7;
  planeMesh.position.x = Math.sin( time * options.speed ) * 100;
  planeMesh.rotation.z = 0.2;
  planeMesh.rotation.y = -Math.atan2( planeMesh.position.z, planeMesh.position.x );
controls.target.copy( planeMesh.position );
}

function onWindowResize() {

   camera.aspect = window.innerWidth / window.innerHeight;
   camera.updateProjectionMatrix();

   renderer.setSize( window.innerWidth, window.innerHeight );

}

window.addEventListener( 'resize', onWindowResize, false );
