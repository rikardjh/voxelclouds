function VoxelMesh( data, w, h, mirrorY ){
  var vCount = ( w * h );

  var itemSize = data.length / vCount;
  var maxY = 0;
  for ( var i = 0; i < data.length; i+= itemSize ){
    maxY = Math.max( maxY, data[ i ] );
  }

  this._vertices = [];
  this._normals  = [];
  this._triangles = [];
  this._colors = [];

  var halfW = w * 0.5;
  var halfH = h * 0.5;

  for ( var i = 0; i < vCount; i++ ){
    var x = i % w;
    var it = i * itemSize;
    var z = Math.floor( i / w );
    var y = Math.round(data[ it ]);

    if ( y ){
      for ( var _y = 0; _y < y; _y++ ){
        var left   = x === 0 || data[ it - itemSize ] !== y;
        var right  = x === w || data.length ||  data[ it + itemSize ] !== y;
        var back   = z === 0 || data[ it - itemSize * w ] !== y;
        var front  = z === h || data[ it + itemSize * w ] !== y;
        var top    = _y === y - 1;
        var bottom = _y === 0;

        this.addVoxel( x - halfW, _y, z - halfH, left, right, bottom, top, back, front );
        if ( mirrorY && _y > 0 ) // make a symetrical copy below, for clouds etc
          this.addVoxel( x - halfW, -_y, z - halfH, left, right, bottom, top, back, front );
      }
    }
  }

}

VoxelMesh.prototype.addVoxel = function( x, y, z, xNeg, xPos, yNeg, yPos, zNeg, zPos ){
  var p1, p2, p3, p4, p5, p6, p7, p8, v1, v2, v3, v4, v5, v6, v7, v8, size;

  size = 0.5;

  p1 = [ x - size, y - size, z + size ];
  p2 = [ x + size, y - size, z + size ];
  p3 = [ x + size, y + size, z + size ];
  p4 = [ x - size, y + size, z + size ];
  p5 = [ x + size, y - size, z - size ];
  p6 = [ x - size, y - size, z - size ];
  p7 = [ x - size, y + size, z - size ];
  p8 = [ x + size, y + size, z - size ];

  // Front
  if ( zPos ){
    n = [ 0, 0, 1 ];

    v1 = this.addVertex( p1, n );
    v2 = this.addVertex( p2, n );
    v3 = this.addVertex( p3, n );
    v4 = this.addVertex( p4, n );

    this.addQuad( v1, v2, v3, v4 );
  }

  // Back
  if ( zNeg ){
    n = [ 0, 0, -1 ];

    v5 = this.addVertex( p5, n );
    v6 = this.addVertex( p6, n );
    v7 = this.addVertex( p7, n );
    v8 = this.addVertex( p8, n );

    this.addQuad( v5, v6, v7, v8 );
  }

  // Right
  if ( xPos ){
    n = [ 1, 0, 0 ];

    v2 = this.addVertex( p2, n );
    v5 = this.addVertex( p5, n );
    v8 = this.addVertex( p8, n );
    v3 = this.addVertex( p3, n );

    this.addQuad( v2, v5, v8, v3 );
  }

  // left
  if ( xNeg ) {
    n = [ -1, 0, 0 ];

    v6 = this.addVertex( p6, n );
    v1 = this.addVertex( p1, n );
    v4 = this.addVertex( p4, n );
    v7 = this.addVertex( p7, n );

    this.addQuad( v6, v1, v4, v7 );
  }

  // Top
  if ( yPos ) {

    n = [ 0, 1, 0 ];

    v4 = this.addVertex( p4, n );
    v3 = this.addVertex( p3, n );
    v8 = this.addVertex( p8, n );
    v7 = this.addVertex( p7, n );

    this.addQuad( v4, v3, v8, v7 );
  }

  // Bottom
  if ( yNeg ) {

    n = [ 0, -1, 0 ];

    v6 = this.addVertex( p6, n );
    v5 = this.addVertex( p5, n );
    v2 = this.addVertex( p2, n );
    v1 = this.addVertex( p1, n );

    this.addQuad( v6, v5, v2, v1 );
  }
}

VoxelMesh.prototype.addQuad = function( v1, v2, v3, v4 ){
  this.addTriangle( v1, v2, v3 );
  this.addTriangle( v1, v3, v4 );
}

VoxelMesh.prototype.addVertex = function( v, n ){
  this._vertices.push( v[0], v[1], v[2] );
  this._normals.push( n[0], n[1], n[2] );
  this._colors.push( 1, 1, 1 );
  return ( this._vertices.length - 3 ) / 3;
};

VoxelMesh.prototype.addTriangle = function( v1, v2, v3 ){
  this._triangles.push( v1, v2, v3 );
};

VoxelMesh.prototype.make = function(){

  if ( this._made ){
    throw new Error( 'Only call VoxelMesh.make once' );
  }
  this._made = true;

  var geometry = new THREE.BufferGeometry();
  geometry.addAttribute( 'position', new THREE.BufferAttribute( new Float32Array( this._vertices ), 3 ) );
  geometry.addAttribute( 'normal', new THREE.BufferAttribute( new Float32Array( this._normals ), 3 ) );
  /** no need for color / uv at this point **/
  //geometry.addAttribute( 'color', new THREE.BufferAttribute(  new Float32Array( this._colors ), 3 ) );
  //geometry.addAttribute( 'uv', new THREE.BufferAttribute( uvs, 2 ) );

  geometry.setIndex( new THREE.BufferAttribute( new Uint16Array( this._triangles ), 1 ));

  // optional
  geometry.computeBoundingBox();
  geometry.computeBoundingSphere();

  // set the normals
  geometry.computeVertexNormals(); // computed vertex normals are orthogonal to the face for non-indexed BufferGeometry
  geometry.computeFaceNormals ();



  // clean up
  this._vertices = null;
  this._normals  = null;
  this._triangles = null;
  this._colors = null;

  return geometry;
};
